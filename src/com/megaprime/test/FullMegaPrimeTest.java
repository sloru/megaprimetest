package com.megaprime.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.megaprime.engine.MegaPrimeFactory;
import com.megaprime.engine.MegaPrimeFactory.MEGA_TYPE;

public class FullMegaPrimeTest {
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void testLeftMegaPrime() {
		assertTrue(MegaPrimeFactory.getMegaPrime(MEGA_TYPE.LEFT).isMegaPrime(
				997));
	}

	@Test
	public void testLeftMegaPrimeFail() {
		assertFalse(MegaPrimeFactory.getMegaPrime(MEGA_TYPE.LEFT).isMegaPrime(
				994));
		assertFalse(MegaPrimeFactory.getMegaPrime(MEGA_TYPE.LEFT).isMegaPrime(
				0));
	}

	@Test
	public void testRightMegaPrime() {
		assertTrue(MegaPrimeFactory.getMegaPrime(MEGA_TYPE.RIGHT).isMegaPrime(
				7393));
	}

	@Test
	public void testRightMegaPrimeFail() {
		assertFalse(MegaPrimeFactory.getMegaPrime(MEGA_TYPE.RIGHT).isMegaPrime(
				8393));
		assertFalse(MegaPrimeFactory.getMegaPrime(MEGA_TYPE.RIGHT).isMegaPrime(
				0));
	}
	
	@Test
	public void testFullMegaPrime() {
		assertTrue(MegaPrimeFactory.getMegaPrime(MEGA_TYPE.FULL).isMegaPrime(
				31));
	}
	
	@Test
	public void testFullMegaPrimeFail() {
		assertFalse(MegaPrimeFactory.getMegaPrime(MEGA_TYPE.FULL).isMegaPrime(
				32));
		assertFalse(MegaPrimeFactory.getMegaPrime(MEGA_TYPE.FULL).isMegaPrime(
				0));
	}
	
	@Test
	public void testFullMegaPrimeException() {
		thrown.expect(IllegalArgumentException.class);
		assertFalse(MegaPrimeFactory.getMegaPrime(MEGA_TYPE.FULL).isMegaPrime(
				-1));
	}
	@Test
	public void testLeftMegaPrimeException() {
		thrown.expect(IllegalArgumentException.class);
		assertFalse(MegaPrimeFactory.getMegaPrime(MEGA_TYPE.LEFT).isMegaPrime(
				-1));
	}
	@Test
	public void testRightMegaPrimeException() {
		thrown.expect(IllegalArgumentException.class);
		assertFalse(MegaPrimeFactory.getMegaPrime(MEGA_TYPE.RIGHT).isMegaPrime(
				-1));
	}
	
	

}
